/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(2);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.flipCanvasOriginals = exports.resetCanvasOriginals = exports.getCanvas = exports.getMainImage = exports.resizeCanvas = exports.canvasHeight = exports.canvasWidth = exports.originalHeight = exports.originalWidth = exports.canvas = exports.imgInstance = exports.currentScaling = exports.addTheImage = undefined;
	
	var _module_paint = __webpack_require__(2);
	
	var paint = _interopRequireWildcard(_module_paint);
	
	var _module_zooming = __webpack_require__(4);
	
	var zoom = _interopRequireWildcard(_module_zooming);
	
	var _module_crop = __webpack_require__(5);
	
	var crop = _interopRequireWildcard(_module_crop);
	
	var _module_rotate = __webpack_require__(6);
	
	var rotatemod = _interopRequireWildcard(_module_rotate);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }
	
	var currentScaling = 1;
	var imgInstance = false;
	var canvas = false;
	var originalWidth = 0;
	var originalHeight = 0;
	var canvasWidth = 0;
	var canvasHeight = 0;
	
	var $container = document.getElementById("container");
	
	function createCanvas(w, h) {
	
	    exports.originalWidth = originalWidth = w;
	    exports.originalHeight = originalHeight = h;
	    exports.canvasWidth = canvasWidth = w;
	    exports.canvasHeight = canvasHeight = h;
	
	    //create canvas
	    exports.canvas = canvas = new fabric.Canvas('drawingCanvas', {
	        width: w,
	        height: h
	    });
	
	    //canvas observe
	
	    canvas.observe('object:added', function (e) {
	        console.log("added", paint);
	        e.target._origStrokeWidth = e.target.strokeWidth;
	
	        paint.updateObjectOriginals(e);
	    });
	
	    canvas.observe('object:moving', function (e) {
	
	        paint.updateObjectOriginals(e);
	    });
	
	    canvas.observe('object:selected', function (e) {
	
	        //
	        document.querySelector('#trash_can').classList.add("active");
	
	        //
	        paint.clearActiveAction();
	
	        //
	        paint.setSelectedStroke(Math.ceil(e.target.strokeWidth));
	    });
	
	    canvas.observe('selection:cleared', function (e) {
	
	        document.querySelector('#trash_can').classList.remove("active");
	
	        paint.setStrokeToCurrentValue();
	    });
	
	    canvas.on('object:scaling', function (e) {
	
	        e.target._scaleX = e.target.scaleX;
	        e.target._scaleY = e.target.scaleY;
	
	        e.target._left = e.target.getLeft();
	        e.target._top = e.target.getTop();
	
	        if (e.target.isclipper === false) {
	            e.target.resizeToScale();
	        }
	    });
	
	    $container.classList.add("opened");
	
	    document.getElementById("save").addEventListener('click', function (e) {
	
	        e.preventDefault();
	
	        saveCanvas();
	    });
	}
	
	function addImageToCanvas(img) {
	
	    exports.imgInstance = imgInstance = img;
	
	    imgInstance.set({
	
	        left: originalWidth / 2,
	        top: originalHeight / 2,
	        angle: 0,
	        opacity: 1,
	        selectable: false,
	        lockScalingX: true,
	        lockScalingY: true,
	        lockMovementX: true,
	        lockMovementY: true,
	        centeredRotation: true,
	        centeredScaling: true,
	        mainImage: true,
	        originX: "center",
	        originY: "center"
	    });
	
	    canvas.add(imgInstance);
	
	    canvas.sendToBack(imgInstance);
	
	    canvas.renderAll();
	}
	
	var addTheImage = function addTheImage(url) {
	
	    //add the image
	    //var imgElement = $('.text-center').find('img#image');
	
	    fabric.Image.fromURL(url, function (img) {
	
	        createCanvas(img.getWidth(), img.getHeight());
	
	        addImageToCanvas(img);
	
	        paint.setCanvas(canvas);
	
	        zoom.listenZoom();
	    });
	};
	
	function resizeCanvas(w, h) {
	
	    var $canvasContainer = document.querySelector('#imageContainer .canvas-container');
	
	    //update variables
	    exports.canvasWidth = canvasWidth = w;
	    exports.canvasHeight = canvasHeight = h;
	
	    //update canvas elements
	    canvas.setDimensions({ width: w, height: h });
	
	    //update CANVASes in dom
	    var cns = $canvasContainer.querySelectorAll('canvas');
	    var cnsarr = [].concat(_toConsumableArray(cns));
	
	    cnsarr.forEach(function (c) {
	        c.style.width = w;
	        c.style.height = h;
	    });
	
	    $canvasContainer.style.width = w;
	    $canvasContainer.style.height = h;
	
	    canvas.clipTo = function (ctx) {
	        ctx.rect(0, 0, w, h);
	    };
	    canvas.renderAll();
	}
	
	function resetCanvasOriginals(width, height) {
	
	    exports.originalWidth = originalWidth = width;
	    exports.originalHeight = originalHeight = height;
	}
	
	function flipCanvasOriginals() {
	
	    var w = originalWidth;
	    var h = originalHeight;
	
	    exports.originalWidth = originalWidth = h;
	    exports.originalHeight = originalHeight = w;
	}
	
	function saveCanvas() {
	
	    var scale = zoom.getZoomFactor();
	
	    var realScale = Math.round(1 / scale * 100) / 100;
	
	    var img = canvas.toDataURL({
	        format: 'png',
	        left: 0,
	        top: 0,
	        width: canvasWidth,
	        height: canvasHeight,
	        multiplier: realScale
	    });
	
	    window.open(img);
	}
	
	/*///  GETTERS &  SETTERS ///*/
	
	function getMainImage() {
	    return imgInstance;
	}
	function getCanvas() {
	    return canvas;
	}
	
	exports.addTheImage = addTheImage;
	exports.currentScaling = currentScaling;
	exports.imgInstance = imgInstance;
	exports.canvas = canvas;
	exports.originalWidth = originalWidth;
	exports.originalHeight = originalHeight;
	exports.canvasWidth = canvasWidth;
	exports.canvasHeight = canvasHeight;
	exports.resizeCanvas = resizeCanvas;
	exports.getMainImage = getMainImage;
	exports.getCanvas = getCanvas;
	exports.resetCanvasOriginals = resetCanvasOriginals;
	exports.flipCanvasOriginals = flipCanvasOriginals;

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.activeAction = exports.setCanvas = exports.setStrokeToCurrentValue = exports.setSelectedStroke = exports.clearActiveAction = exports.updateObjectOriginals = exports.setStrokeWidth = exports.getStrokeWidth = undefined;
	
	var _core = __webpack_require__(1);
	
	var core = _interopRequireWildcard(_core);
	
	var _module_change_stroke_size = __webpack_require__(3);
	
	var stroke = _interopRequireWildcard(_module_change_stroke_size);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }
	
	//keep the initial width & height for reference
	
	////------------------
	
	var _x = 0;
	var _y = 0;
	var _width = 100;
	var _height = 100;
	var _circRadius = 50;
	
	//default border color, when you select the object
	var _borderColor = 'red';
	var _strokeColor = 'rgba(255,0,0,1)';
	var _strokeWidth = 5;
	
	var _cornerSize = 8;
	
	var $drawingBtns = document.getElementById("drawingBtns");
	
	var $colorpickerCanvas = document.getElementById("colorpickerCanvas");
	
	var $strokeSize = document.getElementById('strokeSize');
	
	var $addImage = document.getElementById('add_picture');
	
	var $fileinput = document.getElementById('fileinput');
	
	var activeAction = false;
	
	var canvas = canvas;
	
	// initializing default properties
	fabric.Object.prototype.set({
	    transparentCorners: false,
	    cornerColor: 'rgba(102,153,255,1)',
	    cornerSize: _cornerSize
	});
	
	// customise fabric.Object with a method to resize rather than just scale after tranformation
	fabric.Object.prototype.resizeToScale = function () {
	
	    if (this.type === 'group') {
	        this._objects.forEach(function (obj) {
	
	            obj.strokeWidth = Math.ceil(obj._origStrokeWidth / Math.max(obj.group.scaleX, obj.group.scaleY));
	        });
	    } else {
	        this.strokeWidth = Math.ceil(this._origStrokeWidth / Math.max(this.scaleX, this.scaleY));
	    }
	};
	
	// start
	defineColors();
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//functions
	
	function defineColors() {
	
	    var colorBtns = document.querySelectorAll('#colorPalette button');
	    var colorBtnsSrr = [].concat(_toConsumableArray(colorBtns));
	
	    colorBtnsSrr.forEach(function (el) {
	
	        el.style.backgroundColor = el.value;
	    });
	}
	
	function updateObjectOriginals(e) {
	
	    e.target._left = e.target.getLeft();
	    e.target._top = e.target.getTop();
	    e.target._width = e.target.getWidth();
	    e.target._height = e.target.getHeight();
	    e.target._scaleX = e.target.scaleX;
	    e.target._scaleY = e.target.scaleY;
	}
	
	function setSelectedStroke(nr) {
	
	    if (canvas.getActiveGroup()) {
	
	        //here put -
	        $strokeSize.value = ' ';
	    } else {
	
	        $strokeSize.value = nr;
	    }
	}
	
	function setStrokeToCurrentValue() {
	
	    setSelectedStroke(_strokeWidth);
	}
	
	function disableFreeDraw() {
	
	    canvas.isDrawingMode = false;
	
	    canvas.renderAll();
	}
	
	function clearActiveSubmenu() {
	    //remove current active
	    var active = document.querySelector('.draw_icon.active');
	
	    if (active != null) active.classList.remove('active');
	}
	
	function clearActiveAction() {
	    clearActiveSubmenu();
	    exports.activeAction = activeAction = false;
	    canvas.isDrawingMode = false;
	}
	
	function toggleActive(el) {
	
	    clearActiveSubmenu();
	
	    //add new active
	    el.classList.add('active');
	}
	
	function setStokeForActiveObject(newColor) {
	
	    if (canvas.getActiveObject()) {
	
	        var obj = canvas.getActiveObject();
	
	        if (obj.type === "group") {
	
	            var nr = obj.size() - 1;
	
	            while (nr >= 0) {
	
	                if (obj.item(nr).stroke !== null) {
	                    obj.item(nr).set("stroke", newColor);
	                }
	
	                if (obj.item(nr).fill !== null) {
	                    obj.item(nr).set("fill", newColor);
	                }
	
	                nr--;
	            }
	            canvas.renderAll();
	        } else {
	            if (obj.stroke !== null) {
	                obj.set("stroke", newColor);
	            }
	
	            if (obj.fill !== null) {
	                obj.set("fill", newColor);
	            }
	
	            canvas.renderAll();
	        }
	    } else if (canvas.getActiveGroup()) {
	
	        canvas.getActiveGroup().forEachObject(function (o) {
	
	            o.set("stroke", newColor);
	
	            if (o.fill !== null) {
	                o.set("fill", newColor);
	            }
	        });
	        canvas.renderAll();
	    } else {
	
	        //if there's no object selected
	        //then change the default stroke color
	        _strokeColor = newColor;
	        canvas.freeDrawingBrush.color = newColor;
	    }
	}
	
	function drawText() {
	
	    var itext = new fabric.IText("write here...", {
	        left: _x,
	        top: _y,
	        fontSize: 20,
	        fill: _strokeColor,
	        originX: "center",
	        originY: "center",
	        strokeWidth: 0
	    });
	    canvas.add(itext);
	}
	
	function drawSquare() {
	
	    //first disable free draw mode
	    disableFreeDraw();
	
	    // create a rectangle object
	    var rect = new fabric.Rect({
	        left: _x,
	        top: _y,
	        fill: null,
	        width: _width,
	        height: _height,
	        borderColor: _borderColor,
	        strokeWidth: _strokeWidth,
	        _origStrokeWidth: _strokeWidth,
	        stroke: _strokeColor,
	        originX: "center",
	        originY: "center",
	        'selectable': true
	    });
	
	    // "add" rectangle onto canvas
	    canvas.add(rect);
	}
	
	function drawCircle() {
	    //console.log("draw circle");
	
	    //first disable free draw mode
	    disableFreeDraw();
	
	    // create a rectangle object
	    var Circle = new fabric.Circle({
	        left: _x,
	        top: _y,
	        fill: null,
	        radius: _circRadius,
	        strokeWidth: _strokeWidth,
	        stroke: _strokeColor,
	        borderColor: _borderColor,
	        originX: "center",
	        originY: "center",
	        'selectable': true
	    });
	
	    // "add" rectangle onto canvas
	    canvas.add(Circle);
	
	    canvas.bringToFront(Circle);
	
	    canvas.renderAll();
	}
	
	function drawLine() {
	    //console.log("draw line");
	
	    //first disable free draw mode
	    disableFreeDraw();
	
	    var line = new fabric.Line([0, 100, 0, 0], {
	        left: _x,
	        top: _y,
	        strokeWidth: _strokeWidth,
	        stroke: _strokeColor,
	        borderColor: _borderColor,
	        fill: null,
	        'selectable': true
	    });
	
	    canvas.add(line);
	}
	
	function drawFreely() {
	
	    // **Enables line drawing
	    canvas.isDrawingMode = true;
	
	    //set properties
	    canvas.freeDrawingBrush.width = _strokeWidth;
	    canvas.freeDrawingBrush.color = _strokeColor;
	}
	
	function drawArrow() {
	
	    //first disable free draw mode
	    disableFreeDraw();
	
	    var line, arrow, circle, deltaX, deltaY;
	
	    line = new fabric.Line([0, 0, 100, 0], {
	        stroke: _strokeColor,
	        selectable: true,
	        strokeWidth: '3',
	        padding: 5,
	        hasBorders: false,
	        hasControls: false,
	        originX: 'center',
	        originY: 'center'
	    });
	
	    var centerX = (line.x1 + line.x2) / 2,
	        centerY = (line.y1 + line.y2) / 2;
	    deltaX = line.left - centerX, deltaY = line.top - centerY;
	
	    arrow = new fabric.Triangle({
	        left: line.get('x1') + deltaX,
	        top: line.get('y1') + deltaY,
	        originX: 'center',
	        originY: 'center',
	        hasBorders: false,
	        hasControls: false,
	        pointType: 'arrow_start',
	        strokeWidth: 0,
	        angle: -90,
	        width: 20,
	        height: 20,
	        fill: _strokeColor
	    });
	
	    circle = new fabric.Circle({
	        left: line.get('x2') + deltaX,
	        top: line.get('y2') + deltaY,
	        radius: 3,
	        strokeWidth: 3,
	        originX: 'center',
	        originY: 'center',
	        hasBorders: false,
	        hasControls: false,
	        pointType: 'arrow_end',
	        stroke: _strokeColor
	    });
	
	    line.customType = arrow.customType = circle.customType = 'arrow';
	
	    var group = new fabric.Group([line, circle, arrow], {
	        left: _x,
	        top: _y,
	        strokeWidth: 3
	    });
	
	    canvas.add(group);
	}
	
	function deleteActive() {
	
	    if (canvas.getActiveObject()) {
	
	        var actobj = canvas.getActiveObject();
	
	        canvas.remove(actobj);
	    } else if (canvas.getActiveGroup()) {
	
	        canvas.getActiveGroup().forEachObject(function (o) {
	            canvas.remove(o);
	        });
	        canvas.discardActiveGroup().renderAll();
	    }
	}
	
	document.getElementById('trash_can').addEventListener('click', function () {
	
	    if (this.classList.contains("active")) {
	        deleteActive();
	    }
	});
	
	$drawingBtns.addEventListener('click', function (e) {
	    // console.log(e);
	    e.preventDefault();
	    e.stopPropagation();
	
	    var el = e.target;
	    var id = el.getAttribute('id');
	
	    //if not parent
	    if (id != "drawingBtns") {
	
	        if (el.classList.contains("active")) {
	
	            el.classList.remove("active");
	
	            //reset active action
	            exports.activeAction = activeAction = false;
	
	            disableFreeDraw();
	        } else {
	
	            if (canvas.isDrawingMode == true) {
	                //make sure to always disable free draw
	                disableFreeDraw();
	            }
	
	            toggleActive(el);
	            exports.activeAction = activeAction = id;
	
	            if (id == "add_picture") {}
	        }
	    }
	});
	
	document.getElementById('setColor').addEventListener('click', function (e) {
	
	    if (e.target.tagName == "BUTTON") {
	
	        var el = e.target;
	
	        var newColor = new fabric.Color(el.value).toRgb();
	
	        setStokeForActiveObject(newColor);
	
	        //if there are no selected objects, then set the active color
	        if (!canvas.getActiveObject() && !canvas.getActiveGroup()) {
	
	            var parent = el.parentNode;
	
	            parent.querySelector(".active").classList.remove("active");
	
	            el.classList.add("active");
	        }
	    }
	});
	
	function executeAction() {
	
	    switch (activeAction) {
	
	        case "draw_text":
	            drawText();
	            break;
	
	        case "draw_circle":
	            drawCircle();
	            break;
	
	        case "draw_square":
	            drawSquare();
	            break;
	
	        case "draw_line":
	            drawLine();
	            break;
	
	        case "draw_arrow":
	            drawArrow();
	            break;
	
	        case "draw_freely":
	            drawFreely();
	            break;
	
	        default:
	            break;
	    }
	}
	
	function uploadImage() {
	    console.log('upload');
	}
	
	/*///-----  GETTERS and SETTERS  -----///*/
	
	var getStrokeWidth = function getStrokeWidth() {
	
	    return _strokeWidth;
	};
	var setStrokeWidth = function setStrokeWidth(nr) {
	
	    _strokeWidth = nr;
	};
	
	function setCanvas(_canvas) {
	
	    canvas = _canvas;
	
	    canvas.observe('mouse:down', function (opt) {
	
	        if (activeAction !== false) {
	
	            _x = opt.e.offsetX / core.currentScaling;
	            _y = opt.e.offsetY / core.currentScaling;
	
	            executeAction();
	        }
	    });
	
	    //add stroke listener
	    stroke.addStrokeListener();
	}
	
	$colorpickerCanvas.addEventListener("input", function (e) {
	
	    var newColor = new fabric.Color(this.value).toRgb();
	
	    setStokeForActiveObject(newColor);
	});
	
	$fileinput.addEventListener("change", function (e) {
	
	    //console.log(e, this);
	    //let preview = document.querySelector('img');
	    var file = this.files[0];
	    var reader = new FileReader();
	
	    reader.addEventListener("load", function () {
	        //preview.src = reader.result;
	        core.addTheImage(reader.result);
	
	        //hide the file input
	        e.target.style.display = "none";
	    }, false);
	
	    if (file) {
	        reader.readAsDataURL(file);
	    }
	});
	
	exports.getStrokeWidth = getStrokeWidth;
	exports.setStrokeWidth = setStrokeWidth;
	exports.updateObjectOriginals = updateObjectOriginals;
	exports.clearActiveAction = clearActiveAction;
	exports.setSelectedStroke = setSelectedStroke;
	exports.setStrokeToCurrentValue = setStrokeToCurrentValue;
	exports.setCanvas = setCanvas;
	exports.activeAction = activeAction;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var _core = __webpack_require__(1);
	
	var core = _interopRequireWildcard(_core);
	
	var _module_paint = __webpack_require__(2);
	
	var paint = _interopRequireWildcard(_module_paint);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	var $strokeSize = document.getElementById('strokeSize');
	
	function changeObjectStroke(obj, stVal) {
	
	    var newStroke = Number(stVal) / Math.max(obj.scaleX, obj.scaleY);
	
	    if (obj.type === "group") {
	
	        var nr = obj.size() - 1;
	        while (nr >= 0) {
	            obj.item(nr).strokeWidth = newStroke;
	            nr--;
	        }
	
	        obj._origStrokeWidth = newStroke;
	    } else {
	
	        obj.strokeWidth = newStroke;
	
	        obj._origStrokeWidth = newStroke;
	    }
	}
	
	function addStrokeListener() {
	
	    $strokeSize.addEventListener("change", function (e) {
	
	        console.log(this.value);
	
	        var newVal = this.value;
	
	        var canvas = core.getCanvas();
	
	        var _strokeWidth = paint.getStrokeWidth();
	
	        if (canvas.getActiveObject()) {
	
	            //if there is active element
	            var obj = canvas.getActiveObject();
	
	            changeObjectStroke(obj, newVal);
	
	            //display change
	            canvas.renderAll();
	        } else if (canvas.getActiveGroup()) {
	
	            canvas.getActiveGroup().forEachObject(function (o) {
	
	                changeObjectStroke(o, newVal);
	            });
	            canvas.renderAll();
	        } else {
	            paint.setStrokeWidth(Number(this.value));
	            canvas.freeDrawingBrush.width = _strokeWidth;
	        }
	    });
	}
	
	module.exports = {
	    addStrokeListener: addStrokeListener
	};

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.getZoomFactor = exports.listenZoom = undefined;
	
	var _core = __webpack_require__(1);
	
	var core = _interopRequireWildcard(_core);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	var $currZoom = document.getElementById("currentZoom");
	
	var currFactor = 1;
	
	function listenZoom() {
	
	    var resizeControl = document.getElementById("resizeControl");
	
	    if (typeof resizeControl != "undefined" && resizeControl != null) {
	
	        resizeControl.addEventListener('change', function (result) {
	
	            var delta = Number(this.value);
	
	            var factor = Math.round((1 + delta) * 100) / 100;
	
	            currFactor = factor;
	
	            zoomIt(factor);
	
	            $currZoom.innerText = factor;
	        });
	    }
	}
	/////
	
	function zoomIt(factor) {
	
	    console.log(factor);
	
	    var canvas = core.getCanvas();
	
	    var cwidth = Math.round(core.originalWidth * factor * 100) / 100;
	    var cheight = Math.round(core.originalHeight * factor * 100) / 100;
	
	    if (canvas.backgroundImage) {
	        // Need to scale background images as well
	        var bi = canvas.backgroundImage;
	        bi.width = bi.width * factor;
	        bi.height = bi.height * factor;
	    }
	
	    var objects = canvas.getObjects();
	
	    for (var i in objects) {
	        /*
	        var scaleX = objects[i].scaleX;
	        var scaleY = objects[i].scaleY;
	        var left = objects[i].getLeft();
	        var top = objects[i].getTop();
	        */
	        var _left = objects[i]._left;
	        var _top = objects[i]._top;
	        var _scaleX = objects[i]._scaleX;
	        var _scaleY = objects[i]._scaleY;
	
	        var tempScaleX = Math.round(_scaleX * factor * 100) / 100;
	        var tempScaleY = Math.round(_scaleY * factor * 100) / 100;
	        var tempLeft = Math.round(_left * factor * 100) / 100;
	        var tempTop = Math.round(_top * factor * 100) / 100;
	
	        console.log(_scaleX, _scaleY, tempScaleX, tempScaleY);
	
	        if (!objects[i].isclipper) {
	
	            objects[i].setLeft(tempLeft);
	            objects[i].setTop(tempTop);
	
	            objects[i].scaleX = tempScaleX;
	            objects[i].scaleY = tempScaleY;
	        }
	
	        objects[i].setCoords();
	    }
	
	    canvas.calcOffset();
	    canvas.renderAll();
	
	    core.resizeCanvas(cwidth, cheight);
	}
	
	function getZoomFactor() {
	
	    return currFactor;
	}
	
	exports.listenZoom = listenZoom;
	exports.getZoomFactor = getZoomFactor;

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.removeCropping = exports.addCropRectangle = undefined;
	
	var _core = __webpack_require__(1);
	
	var core = _interopRequireWildcard(_core);
	
	var _module_paint = __webpack_require__(2);
	
	var paint = _interopRequireWildcard(_module_paint);
	
	var _module_zooming = __webpack_require__(4);
	
	var zoom = _interopRequireWildcard(_module_zooming);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	var cropRect = false;
	
	var cropOffset = 0;
	
	var isScaling = false;
	
	var _lastScaleEventTime = 0;
	
	var _cleft = void 0,
	    _ctop = void 0,
	    _cwidth = void 0,
	    _cheight = void 0,
	    canvas = void 0,
	    canvasWidth = void 0,
	    canvasHeight = void 0,
	    rectTop = void 0,
	    rectBottom = void 0,
	    rectLeft = void 0,
	    rectRight = void 0;
	
	var $crop = document.getElementById('crop');
	var $cancelCrop = document.getElementById('cancelCrop');
	var $applyCrop = document.getElementById('applyCrop');
	var $subMenu = document.getElementById('subMenu');
	
	var $container = document.getElementById('container');
	
	$crop.addEventListener('click', function (e) {
	
	    if (cropRect == false) {
	
	        addCropRectangle();
	
	        $subMenu.style.display = "block";
	
	        $container.classList.add("crop");
	    }
	});
	
	$cancelCrop.addEventListener('click', function (e) {
	
	    removeCropping();
	});
	
	$applyCrop.addEventListener('click', function (e) {
	
	    //reposition all objects based on crop area
	    positionObjects();
	
	    //crop and resize the canvas
	    cropCanvas();
	});
	
	function addCropRectangle() {
	
	    canvas = core.getCanvas();
	    canvasWidth = canvas.getWidth();
	    canvasHeight = canvas.getHeight();
	
	    _cleft = cropOffset;
	    _ctop = cropOffset;
	    _cwidth = canvasWidth - cropOffset * 2;
	    _cheight = canvasHeight - cropOffset * 2;
	
	    cropRect = new fabric.Rect({
	        left: _cleft,
	        top: _ctop,
	        fill: '',
	        width: _cwidth,
	        height: _cheight,
	        lockRotation: true,
	        borderColor: '',
	        cornerColor: 'red',
	        cornerSize: 16,
	        transparentCorners: false,
	        selectable: true,
	        lockScalingFlip: true,
	        isclipper: true,
	        centeredScaling: false
	    });
	
	    canvas.add(cropRect);
	
	    createShadowRects();
	
	    canvas.renderAll();
	
	    canvas.bringToFront(cropRect).setActiveObject(cropRect);
	
	    cropRect.on('scaling', function (event) {
	
	        var diff = event.e.timeStamp - _lastScaleEventTime;
	
	        if (diff > 20) {
	
	            _lastScaleEventTime = event.e.timeStamp;
	            calcCroppingScale(this, event);
	
	            updateShadowRects(this);
	        }
	    });
	    cropRect.on('moving', function () {
	
	        calcCroppingMove(this);
	    });
	}
	
	function createShadowRects() {
	
	    var _top = cropRect.getTop() - cropOffset;
	    var _left = cropRect.getLeft() - cropOffset;
	    var _width = cropRect.getWidth() + cropOffset * 2;
	    var _height = cropRect.getHeight() + cropOffset * 2;
	
	    var opts = {
	        fill: "rgba(0,0,0,0.8)",
	        lockRotation: true,
	        transparentCorners: true,
	        selectable: false,
	        lockScalingFlip: true,
	        centeredScaling: false
	    };
	
	    rectLeft = new fabric.Rect({
	        left: 0,
	        top: 0,
	        width: _left,
	        height: canvasHeight
	    });
	
	    rectTop = new fabric.Rect({
	        left: 0,
	        top: 0,
	        width: canvasWidth,
	        height: _top,
	        originY: "top",
	        originX: "left"
	    });
	
	    rectBottom = new fabric.Rect({
	        left: 0,
	        top: _top + _height,
	        width: canvasWidth,
	        height: canvasHeight - (_top + _height),
	        originY: "bottom"
	    });
	
	    rectRight = new fabric.Rect({
	        left: _left + _width,
	        top: 0,
	        width: canvasWidth - (_left + _width),
	        height: canvasHeight,
	        originX: "left"
	    });
	
	    rectTop.set(opts);
	    rectBottom.set(opts);
	    rectLeft.set(opts);
	    rectRight.set(opts);
	
	    canvas.add(rectTop).add(rectBottom).add(rectLeft).add(rectRight);
	
	    canvas.renderAll();
	}
	
	function updateShadowRects(ob) {
	
	    var _top = cropRect.getTop() - cropOffset;
	    var _left = cropRect.getLeft() - cropOffset;
	    var _width = cropRect.getWidth() + cropOffset * 2;
	    var _height = cropRect.getHeight() + cropOffset * 2;
	
	    //----  top rectangle
	    rectTop.setHeight(round2dec(_top));
	    rectTop.setCoords();
	
	    //----  bottom rectangle
	    rectBottom.set({
	        height: round2dec(canvasHeight - (_top + _height))
	    });
	    rectBottom.setCoords();
	
	    //----  left rectangle
	    rectLeft.setWidth(round2dec(_left));
	    rectLeft.setCoords();
	
	    //----  right rectangle
	    var rleft = round2dec(_left + _width);
	    rectRight.set({
	        width: canvasWidth - rleft,
	        left: rleft
	    });
	    rectRight.setCoords();
	
	    canvas.renderAll();
	}
	
	function calcCroppingScale(ob, event) {
	    // than bound to canvas size
	    //don't let it scale beyond
	    var cw = canvas.getWidth();
	    var ch = canvas.getHeight();
	
	    var pointer = canvas.getPointer(event.e);
	    var posX = pointer.x;
	    var posY = pointer.y;
	
	    if (posY - cropOffset < 0 || posY + cropOffset > ch) {
	
	        ob.lockScalingY = true;
	
	        //console.log(ob);
	
	        if (posY - cropOffset < 0) {
	            ob.set({
	                top: cropOffset
	            });
	
	            ob.setCoords();
	
	            if (ob.getHeight() > ch) {
	                ob.scaleToHeight(ch - cropOffset * 2);
	                ob.set({
	                    scaleX: ob.originalState.scaleX
	                });
	                ob.setCoords();
	            }
	        }
	    } else {
	        ob.lockScalingY = false;
	    }
	
	    if (posX - cropOffset < 0 || posX + cropOffset > cw) {
	
	        ob.lockScalingX = true;
	
	        if (posX - cropOffset < 0) {
	
	            ob.set({
	                left: cropOffset
	            });
	
	            ob.setCoords();
	
	            if (ob.getHeight() > cw) {
	                ob.scaleToWidth(cw - cropOffset * 2);
	                ob.set({
	                    scaleY: ob.originalState.scaleY
	                });
	                ob.setCoords();
	            }
	        }
	
	        if (posX + cropOffset > cw) {
	
	            ob.scaleToWidth(cw - cropOffset * 2);
	        }
	    } else {
	        ob.lockScalingX = false;
	    }
	}
	
	function calcCroppingMove(ob) {
	
	    var obw = Math.round(ob.width * ob.scaleX);
	    var obh = Math.round(ob.height * ob.scaleY);
	
	    var cw = canvas.getWidth();
	    var ch = canvas.getHeight();
	
	    //constrain movement top bottom
	    if (ob.top - cropOffset < 0) {
	        ob.top = cropOffset;
	    }
	    if (obh + ob.top + cropOffset * 2 >= ch) {
	        ob.top = ch - obh - cropOffset;
	    }
	
	    //constrain movement left right
	    if (ob.left - cropOffset < 0) {
	        ob.left = cropOffset;
	    } else if (obw + ob.left + cropOffset * 2 >= cw) {
	        ob.left = cw - obw - cropOffset;
	    }
	
	    ob.setCoords();
	    updateShadowRects(ob);
	
	    canvas.renderAll();
	}
	
	function positionObjects() {
	
	    //first deactive all objects
	    canvas.deactivateAll();
	
	    var leftCrop = cropRect.getLeft();
	    var topCrop = cropRect.getTop();
	
	    var zoomFactor = zoom.getZoomFactor();
	
	    if (canvas.backgroundImage) {
	        // Need to scale background images as well
	        var bi = canvas.backgroundImage;
	        bi.width = bi.width * factor;
	        bi.height = bi.height * factor;
	    }
	
	    var objects = canvas.getObjects();
	
	    var _iteratorNormalCompletion = true;
	    var _didIteratorError = false;
	    var _iteratorError = undefined;
	
	    try {
	        for (var _iterator = objects[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	            var o = _step.value;
	
	
	            var left = o.getLeft();
	            var top = o.getTop();
	
	            var tempLeft = round2dec(left - leftCrop);
	            var tempTop = round2dec(top - topCrop);
	
	            o.setLeft(tempLeft);
	            o.setTop(tempTop);
	
	            //update objects oroginal left top
	            o._left = round2dec(tempLeft / zoomFactor);
	            o._top = round2dec(tempTop / zoomFactor);
	
	            o.setCoords();
	        }
	    } catch (err) {
	        _didIteratorError = true;
	        _iteratorError = err;
	    } finally {
	        try {
	            if (!_iteratorNormalCompletion && _iterator.return) {
	                _iterator.return();
	            }
	        } finally {
	            if (_didIteratorError) {
	                throw _iteratorError;
	            }
	        }
	    }
	
	    canvas.calcOffset();
	    canvas.renderAll();
	}
	
	function cropCanvas() {
	
	    var zoomFactor = zoom.getZoomFactor();
	
	    if (cropRect == null || typeof cropRect == "undefined") {
	        return false;
	    }
	    _cwidth = round2dec(cropRect.getWidth() + cropOffset * 2);
	    _cheight = round2dec(cropRect.getHeight() + cropOffset * 2);
	
	    // clips the canvas based on this rectangle area
	    canvas.clipTo = function (ctx) {
	        ctx.rect(0, 0, _cwidth, _cheight);
	    };
	
	    // remove cropping rectangle
	    removeCropping();
	
	    // remove
	    core.resizeCanvas(_cwidth, _cheight);
	    core.resetCanvasOriginals(round2dec(_cwidth / zoomFactor), round2dec(_cheight / zoomFactor));
	
	    canvas.renderAll();
	
	    canvas = core.getCanvas();
	}
	
	function removeCropping() {
	
	    paint.clearActiveAction();
	
	    if (cropRect != false) {
	
	        cropRect.selectable = false;
	
	        cropRect.off('scaling');
	        cropRect.off('moving');
	
	        canvas.remove(cropRect);
	        canvas.remove(rectTop);
	        canvas.remove(rectBottom);
	        canvas.remove(rectLeft);
	        canvas.remove(rectRight);
	
	        cropRect = false;
	
	        rectTop = null;
	        rectLeft = null;
	        rectBottom = null;
	        rectRight = null;
	
	        $subMenu.style.display = "none";
	
	        $container.styleList.remove("crop");
	    }
	
	    canvas.deactivateAllWithDispatch().renderAll();
	}
	
	function round2dec(nr) {
	
	    return Math.round(nr * 100) / 100;
	}
	
	exports.addCropRectangle = addCropRectangle;
	exports.removeCropping = removeCropping;

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var _core = __webpack_require__(1);
	
	var core = _interopRequireWildcard(_core);
	
	var _module_zooming = __webpack_require__(4);
	
	var zoom = _interopRequireWildcard(_module_zooming);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	document.getElementById('rotateRight').addEventListener('click', function (e) {
	
	    rotateImage(90);
	});
	
	// ------- functions
	
	function rotateImage(degree) {
	
	    /*
	     the canvas itself normally shouldn't change size or rotate
	     but we are changing the size of the of the canvas to fit the image every time the image rotates
	     when canvas changes size all images inside are repositioned in propotion to the new width and height
	      1: we need to rotate and reposition all elements
	     2: reset the cavas width/height based on the rotation
	     */
	
	    var mainImage = core.getMainImage();
	
	    var canvas = core.getCanvas();
	
	    var currWidth = canvas.getWidth();
	    var currHeight = canvas.getHeight();
	
	    var newHeight = currWidth;
	    var newWidth = currHeight;
	
	    var newdeg = mainImage.getAngle() + degree;
	
	    var currZoomFactor = zoom.getZoomFactor();
	
	    var objects = canvas.getObjects();
	
	    var _iteratorNormalCompletion = true;
	    var _didIteratorError = false;
	    var _iteratorError = undefined;
	
	    try {
	        for (var _iterator = objects[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	            var obj = _step.value;
	
	
	            obj.set({
	                originX: "center",
	                originY: "center",
	                centeredRotation: true
	            });
	
	            var imgNewTop = 0;
	            var imgNewLeft = 0;
	
	            var imgLeft = obj.getLeft();
	            var imgTop = obj.getTop();
	
	            //the old bottom serves as new left
	            imgNewLeft = Math.round((newWidth - imgTop) * 100) / 100;
	
	            //left becomes top
	            imgNewTop = Math.round(imgLeft * 100) / 100;
	
	            if (newdeg == 360) {
	                newdeg = 0;
	            }
	
	            obj.setLeft(imgNewLeft);
	            obj.setTop(imgNewTop);
	
	            //update original top left needed in zooming
	            obj._left = Math.round(imgNewLeft / currZoomFactor * 100) / 100;
	            obj._top = Math.round(imgNewTop / currZoomFactor * 100) / 100;
	
	            obj.setAngle(newdeg);
	            obj.setCoords();
	        }
	
	        //2. resize canvas first
	    } catch (err) {
	        _didIteratorError = true;
	        _iteratorError = err;
	    } finally {
	        try {
	            if (!_iteratorNormalCompletion && _iterator.return) {
	                _iterator.return();
	            }
	        } finally {
	            if (_didIteratorError) {
	                throw _iteratorError;
	            }
	        }
	    }
	
	    core.resizeCanvas(newWidth, newHeight);
	
	    core.flipCanvasOriginals();
	
	    canvas.renderAll();
	}

/***/ }
/******/ ]);
//# sourceMappingURL=module_paint.entry.js.map