/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(2);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var _module_zooming = __webpack_require__(2);
	
	var zoom = _interopRequireWildcard(_module_zooming);
	
	var _module_paint = __webpack_require__(3);
	
	var paint = _interopRequireWildcard(_module_paint);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }
	
	var currentScaling = 1;
	var imgInstance = false;
	var canvas = false;
	var originalWidth = 0;
	var originalHeight = 0;
	var canvasWidth = 0;
	var canvasHeight = 0;
	
	function createCanvas(w, h) {
	
	    originalWidth = w;
	    originalHeight = h;
	    canvasWidth = w;
	    canvasHeight = h;
	
	    //create canvas
	    canvas = new fabric.Canvas('drawingCanvas', {
	        width: w,
	        height: h
	    });
	
	    //canvas observe
	
	    canvas.observe('object:added', function (e) {
	        console.log("added", paint);
	        e.target._origStrokeWidth = e.target.strokeWidth;
	
	        paint.updateObjectOriginals(e);
	    });
	
	    canvas.observe('object:moving', function (e) {
	
	        paint.updateObjectOriginals(e);
	    });
	
	    canvas.observe('object:selected', function (e) {
	
	        //
	        document.querySelector('#trash_can').classList.add("active");
	
	        //
	        paint.clearActiveAction();
	
	        //
	        paint.setSelectedStroke(Math.ceil(e.target.strokeWidth));
	    });
	
	    canvas.observe('selection:cleared', function (e) {
	
	        document.querySelector('#trash_can').classList.remove("active");
	
	        paint.setStrokeToCurrentValue();
	    });
	
	    canvas.on('object:scaling', function (e) {
	
	        e.target._scaleX = e.target.scaleX;
	        e.target._scaleY = e.target.scaleY;
	
	        e.target._left = e.target.getLeft();
	        e.target._top = e.target.getTop();
	
	        if (e.target.isclipper === false) {
	            e.target.resizeToScale();
	        }
	    });
	}
	
	function addImageToCanvas(img) {
	
	    imgInstance = img;
	
	    imgInstance.set({
	
	        left: originalWidth / 2,
	        top: originalHeight / 2,
	        angle: 0,
	        opacity: 1,
	        selectable: false,
	        lockScalingX: true,
	        lockScalingY: true,
	        lockMovementX: true,
	        lockMovementY: true,
	        centeredRotation: true,
	        centeredScaling: true,
	        originX: "center",
	        originY: "center"
	    });
	
	    canvas.add(imgInstance);
	
	    canvas.sendToBack(imgInstance);
	
	    canvas.renderAll();
	}
	
	var addTheImage = function addTheImage(url) {
	
	    //add the image
	    //var imgElement = $('.text-center').find('img#image');
	
	    fabric.Image.fromURL(url, function (img) {
	
	        createCanvas(img.getWidth(), img.getHeight());
	
	        addImageToCanvas(img);
	
	        paint.setCanvas(canvas);
	
	        zoom.listenZoom();
	    });
	};
	
	function resizeCanvas(w, h) {
	
	    var $canvasContainer = document.querySelector('#imageContainer .canvas-container');
	
	    //update variables
	    canvasWidth = w;
	    canvasHeight = h;
	
	    //update canvas elements
	    canvas.setDimensions({ width: w, height: h });
	
	    //update dom
	    var cns = $canvasContainer.querySelectorAll('canvas');
	    var cnsarr = [].concat(_toConsumableArray(cns));
	
	    cnsarr.forEach(function (c) {
	
	        c.style.width = w;
	        c.style.height = h;
	    });
	
	    $canvasContainer.style.width = w;
	    $canvasContainer.style.height = h;
	
	    canvas.renderAll();
	}
	
	/*///  GETTERS &  SETTERS ///*/
	
	function getMainImage() {
	    return imgInstance;
	}
	function getCanvas() {
	    return canvas;
	}
	
	module.exports = {
	
	    addTheImage: addTheImage,
	    currentScaling: currentScaling,
	    imgInstance: imgInstance,
	    canvas: canvas,
	    originalWidth: originalWidth,
	    originalHeight: originalHeight,
	    canvasWidth: canvasWidth,
	    canvasHeight: canvasHeight,
	    resizeCanvas: resizeCanvas,
	    getMainImage: getMainImage,
	    getCanvas: getCanvas
	};

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.listenZoom = undefined;
	
	var _module_canvas = __webpack_require__(1);
	
	var core = _interopRequireWildcard(_module_canvas);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	console.log(core);
	
	function listenZoom() {
	
	    console.log("listen zoom", core);
	    /*
	        let resizeControl = document.getElementById("resizeControl");
	    
	    
	        if (typeof resizeControl != "undefined" && resizeControl != null) {
	    
	            resizeControl.addEventListener('change', function(result) {
	    
	    
	                let delta = this.value;
	    
	    
	                console.log( delta );
	    
	                if(delta > 0){
	    
	    
	                    zoomIt(  delta );
	    
	                }else{
	    
	                    zoomIt(  1 / delta  );
	    
	                }
	    
	                //clipCanvas();
	    
	            });
	        }
	    */
	}
	/////
	/*
	function zoomIt( factor ) {
	
	
	    let canvas = core.getCanvas();
	
	    //canvas.setHeight(canvas.getHeight() * factor);
	    //canvas.setWidth(canvas.getWidth() * factor);
	
	    //update variables
	    //canvasWidth = canvas.getHeight() * factor;
	    //canvasHeight= canvas.getWidth() * factor;
	
	    if (canvas.backgroundImage) {
	        // Need to scale background images as well
	        var bi = canvas.backgroundImage;
	        bi.width = bi.width * factor;
	        bi.height = bi.height * factor;
	    }
	
	    var objects = canvas.getObjects();
	
	    for (var i in objects) {
	
	        var scaleX = objects[i].scaleX;
	        var scaleY = objects[i].scaleY;
	        var left = objects[i].getLeft();
	        var top = objects[i].getTop();
	        var _left = objects[i]._left;
	        var _top = objects[i]._top;
	        var _scaleX= objects[i]._scaleX;
	        var _scaleY = objects[i]._scaleY;
	
	        var tempScaleX  = Math.round( (_scaleX * factor) * 100) / 100;
	        var tempScaleY  = Math.round( ( _scaleY * factor) * 100) / 100;
	        var tempLeft    = Math.round( (_left * factor) * 100) / 100;
	        var tempTop     = Math.round( (_top * factor) * 100) / 100;
	
	        if(tempScaleX < 1) tempScaleX = 1;
	        if(tempScaleY < 1) tempScaleY = 1;
	
	
	        if( objects[i].isclipper ){
	            objects[i].left = Math.round( (tempLeft - clipOffset*factor) * 100) / 100;
	            objects[i].top = Math.round( (tempTop  - clipOffset*factor) * 100) / 100;
	        }
	        else{
	            objects[i].left = tempLeft ;
	            objects[i].top = tempTop ;
	
	            objects[i].scaleX = tempScaleX;
	            objects[i].scaleY = tempScaleY;
	        }
	
	        objects[i].setCoords();
	    }
	
	    //canvas.centerObject(imgInstance);
	    //imgInstance.setCoords();
	
	    //imgInstance.selectable = true;
	
	    //optimise clipping rect
	
	    canvas.calcOffset();
	    canvas.renderAll();
	
	}
	*/
	exports.listenZoom = listenZoom;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.activeAction = exports.setCanvas = exports.setStrokeToCurrentValue = exports.setSelectedStroke = exports.clearActiveAction = exports.updateObjectOriginals = exports.setStrokeWidth = exports.getStrokeWidth = undefined;
	
	var _core = __webpack_require__(4);
	
	var core = _interopRequireWildcard(_core);
	
	var _module_change_stroke_size = __webpack_require__(5);
	
	var stroke = _interopRequireWildcard(_module_change_stroke_size);
	
	var _module_canvas = __webpack_require__(1);
	
	var canvasmod = _interopRequireWildcard(_module_canvas);
	
	var _module_rotate = __webpack_require__(6);
	
	var rotatemod = _interopRequireWildcard(_module_rotate);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }
	
	//keep the initial width & height for reference
	
	////------------------
	
	var _x = 0;
	var _y = 0;
	var _width = 100;
	var _height = 100;
	var _circRadius = 50;
	
	//default border color, when you select the object
	var _borderColor = 'red';
	var _strokeColor = 'rgba(255,0,0,1)';
	var _strokeWidth = 5;
	
	var _cornerSize = 12;
	
	var $drawingBtns = document.getElementById("drawingBtns");
	
	var $colorpickerCanvas = document.getElementById("colorpickerCanvas");
	
	var $strokeSize = document.getElementById('strokeSize');
	
	var $addImage = document.getElementById('add_picture');
	
	var $fileinput = document.getElementById('fileinput');
	
	var activeAction = false;
	
	var canvas = canvas;
	
	// initializing default properties
	fabric.Object.prototype.set({
	    transparentCorners: false,
	    cornerColor: 'rgba(102,153,255,1)',
	    cornerSize: _cornerSize
	});
	
	// customise fabric.Object with a method to resize rather than just scale after tranformation
	fabric.Object.prototype.resizeToScale = function () {
	
	    if (this.type === 'group') {
	        this._objects.forEach(function (obj) {
	
	            obj.strokeWidth = Math.ceil(obj._origStrokeWidth / Math.max(obj.group.scaleX, obj.group.scaleY));
	        });
	    } else {
	        this.strokeWidth = Math.ceil(this._origStrokeWidth / Math.max(this.scaleX, this.scaleY));
	    }
	};
	
	// start
	defineColors();
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//functions
	
	function defineColors() {
	
	    var colorBtns = document.querySelectorAll('#colorPalette button');
	    var colorBtnsSrr = [].concat(_toConsumableArray(colorBtns));
	
	    colorBtnsSrr.forEach(function (el) {
	
	        el.style.backgroundColor = el.value;
	    });
	}
	
	function updateObjectOriginals(e) {
	
	    e.target._left = e.target.getLeft();
	    e.target._top = e.target.getTop();
	    e.target._width = e.target.getWidth();
	    e.target._height = e.target.getHeight();
	    e.target._scaleX = e.target.scaleX;
	    e.target._scaleY = e.target.scaleY;
	}
	
	function setSelectedStroke(nr) {
	
	    if (canvas.getActiveGroup()) {
	
	        //here put -
	        $strokeSize.value = ' ';
	    } else {
	
	        $strokeSize.value = nr;
	    }
	}
	
	function setStrokeToCurrentValue() {
	
	    setSelectedStroke(_strokeWidth);
	}
	
	function disableFreeDraw() {
	
	    canvas.isDrawingMode = false;
	
	    canvas.renderAll();
	}
	
	function clearActiveSubmenu() {
	    //remove current active
	    var active = document.querySelector('.draw_icon.active');
	
	    if (active != null) active.classList.remove('active');
	}
	
	function clearActiveAction() {
	    clearActiveSubmenu();
	    exports.activeAction = activeAction = false;
	    canvas.isDrawingMode = false;
	}
	
	function toggleActive(el) {
	
	    clearActiveSubmenu();
	
	    //add new active
	    el.classList.add('active');
	}
	
	function setStokeForActiveObject(newColor) {
	
	    if (canvas.getActiveObject()) {
	
	        var obj = canvas.getActiveObject();
	
	        if (obj.type === "group") {
	
	            var nr = obj.size() - 1;
	
	            while (nr >= 0) {
	
	                if (obj.item(nr).stroke !== null) {
	                    obj.item(nr).set("stroke", newColor);
	                }
	
	                if (obj.item(nr).fill !== null) {
	                    obj.item(nr).set("fill", newColor);
	                }
	
	                nr--;
	            }
	            canvas.renderAll();
	        } else {
	            if (obj.stroke !== null) {
	                obj.set("stroke", newColor);
	            }
	
	            if (obj.fill !== null) {
	                obj.set("fill", newColor);
	            }
	
	            canvas.renderAll();
	        }
	    } else if (canvas.getActiveGroup()) {
	
	        canvas.getActiveGroup().forEachObject(function (o) {
	
	            o.set("stroke", newColor);
	
	            if (o.fill !== null) {
	                o.set("fill", newColor);
	            }
	        });
	        canvas.renderAll();
	    } else {
	
	        //if there's no object selected
	        //then change the default stroke color
	        _strokeColor = newColor;
	        canvas.freeDrawingBrush.color = newColor;
	    }
	}
	
	function drawText() {
	
	    var itext = new fabric.IText("write here...", {
	        left: _x,
	        top: _y,
	        fontSize: 20,
	        fill: _strokeColor,
	        strokeWidth: 0
	    });
	    canvas.add(itext);
	}
	
	function drawSquare() {
	
	    //first disable free draw mode
	    disableFreeDraw();
	
	    // create a rectangle object
	    var rect = new fabric.Rect({
	        left: _x,
	        top: _y,
	        fill: null,
	        width: _width,
	        height: _height,
	        borderColor: _borderColor,
	        strokeWidth: _strokeWidth,
	        _origStrokeWidth: _strokeWidth,
	        stroke: _strokeColor,
	        'selectable': true
	    });
	
	    // "add" rectangle onto canvas
	    canvas.add(rect);
	}
	
	function drawCircle() {
	    console.log("draw circle");
	
	    //first disable free draw mode
	    disableFreeDraw();
	
	    // create a rectangle object
	    var Circle = new fabric.Circle({
	        left: _x,
	        top: _y,
	        fill: null,
	        radius: _circRadius,
	        strokeWidth: _strokeWidth,
	        stroke: _strokeColor,
	        borderColor: _borderColor,
	        'selectable': true
	    });
	
	    // "add" rectangle onto canvas
	    canvas.add(Circle);
	
	    canvas.bringToFront(Circle);
	
	    canvas.renderAll();
	}
	
	function drawLine() {
	    console.log("draw line");
	
	    //first disable free draw mode
	    disableFreeDraw();
	
	    var line = new fabric.Line([0, 100, 0, 0], {
	        left: _x,
	        top: _y,
	        strokeWidth: _strokeWidth,
	        stroke: _strokeColor,
	        borderColor: _borderColor,
	        fill: null,
	        'selectable': true
	    });
	
	    canvas.add(line);
	}
	
	function drawFreely() {
	
	    canvas.isDrawingMode = true; // **Enables line drawing
	    canvas.freeDrawingBrush.width = _strokeWidth;
	    canvas.freeDrawingBrush.color = _strokeColor;
	
	    //Adding code to alter line properties
	
	    canvas.on('path:created', function (e) {
	        if (canvas.isDrawingMode == true) {
	            var line1 = e.path;
	            line1.lockScalingX;
	            line1.lockScalingY;
	        }
	    });
	}
	
	function drawArrow() {
	
	    //first disable free draw mode
	    disableFreeDraw();
	
	    var line, arrow, circle, deltaX, deltaY;
	
	    line = new fabric.Line([0, 0, 100, 0], {
	        stroke: _strokeColor,
	        selectable: true,
	        strokeWidth: '3',
	        padding: 5,
	        hasBorders: false,
	        hasControls: false,
	        originX: 'center',
	        originY: 'center'
	    });
	
	    var centerX = (line.x1 + line.x2) / 2,
	        centerY = (line.y1 + line.y2) / 2;
	    deltaX = line.left - centerX, deltaY = line.top - centerY;
	
	    arrow = new fabric.Triangle({
	        left: line.get('x1') + deltaX,
	        top: line.get('y1') + deltaY,
	        originX: 'center',
	        originY: 'center',
	        hasBorders: false,
	        hasControls: false,
	        pointType: 'arrow_start',
	        strokeWidth: 0,
	        angle: -90,
	        width: 20,
	        height: 20,
	        fill: _strokeColor
	    });
	
	    circle = new fabric.Circle({
	        left: line.get('x2') + deltaX,
	        top: line.get('y2') + deltaY,
	        radius: 3,
	        strokeWidth: 3,
	        originX: 'center',
	        originY: 'center',
	        hasBorders: false,
	        hasControls: false,
	        pointType: 'arrow_end',
	        stroke: _strokeColor
	    });
	
	    line.customType = arrow.customType = circle.customType = 'arrow';
	
	    var group = new fabric.Group([line, circle, arrow], {
	        left: _x,
	        top: _y,
	        strokeWidth: 3
	    });
	
	    canvas.add(group);
	}
	
	function deleteActive() {
	
	    if (canvas.getActiveObject()) {
	
	        var actobj = canvas.getActiveObject();
	
	        canvas.remove(actobj);
	    } else if (canvas.getActiveGroup()) {
	
	        canvas.getActiveGroup().forEachObject(function (o) {
	            canvas.remove(o);
	        });
	        canvas.discardActiveGroup().renderAll();
	    }
	}
	
	document.getElementById('trash_can').addEventListener('click', function () {
	
	    if (this.classList.contains("active")) {
	        deleteActive();
	    }
	});
	
	$drawingBtns.addEventListener('click', function (e) {
	    // console.log(e);
	    e.preventDefault();
	    e.stopPropagation();
	
	    var el = e.target;
	    var id = el.getAttribute('id');
	
	    //if not parent
	    if (id != "drawingBtns") {
	
	        if (el.classList.contains("active")) {
	
	            el.classList.remove("active");
	
	            //reset active action
	            exports.activeAction = activeAction = false;
	
	            disableFreeDraw();
	        } else {
	
	            if (canvas.isDrawingMode == true) {
	                //make sure to always disable free draw
	                disableFreeDraw();
	            }
	
	            toggleActive(el);
	            exports.activeAction = activeAction = id;
	
	            if (id == "add_picture") {}
	        }
	    }
	});
	
	document.getElementById('setColor').addEventListener('click', function (e) {
	
	    if (e.target.tagName == "BUTTON") {
	
	        var el = e.target;
	
	        var newColor = new fabric.Color(el.value).toRgb();
	
	        setStokeForActiveObject(newColor);
	
	        //if there are no selected objects, then set the active color
	        if (!canvas.getActiveObject() && !canvas.getActiveGroup()) {
	
	            var parent = el.parentNode;
	
	            parent.querySelector(".active").classList.remove("active");
	
	            el.classList.add("active");
	        }
	    }
	});
	
	function executeAction() {
	
	    switch (activeAction) {
	
	        case "draw_text":
	            drawText();
	            break;
	
	        case "draw_circle":
	            drawCircle();
	            break;
	
	        case "draw_square":
	            drawSquare();
	            break;
	
	        case "draw_line":
	            drawLine();
	            break;
	
	        case "draw_arrow":
	            drawArrow();
	            break;
	
	        case "draw_freely":
	            drawFreely();
	            break;
	
	        default:
	            break;
	    }
	}
	
	function uploadImage() {
	    console.log('upload');
	}
	
	/*///-----  GETTERS and SETTERS  -----///*/
	
	var getStrokeWidth = function getStrokeWidth() {
	
	    return _strokeWidth;
	};
	var setStrokeWidth = function setStrokeWidth(nr) {
	
	    _strokeWidth = nr;
	};
	
	function setCanvas(_canvas) {
	
	    canvas = _canvas;
	
	    canvas.observe('mouse:down', function (opt) {
	        console.log("click", activeAction, opt);
	
	        // if(typeof opt.target == "undefined" || opt.target == null){
	        // Then refer to
	        console.log("click", activeAction);
	        if (activeAction !== false) {
	
	            _x = opt.e.offsetX / canvasmod.currentScaling;
	            _y = opt.e.offsetY / canvasmod.currentScaling;
	
	            executeAction();
	        }
	        //}
	    });
	
	    //add stroke listener
	    stroke.addStrokeListener();
	}
	
	$colorpickerCanvas.addEventListener("input", function (e) {
	
	    var newColor = new fabric.Color(this.value).toRgb();
	
	    setStokeForActiveObject(newColor);
	});
	
	$fileinput.addEventListener("change", function (e) {
	
	    console.log(e, this);
	    //let preview = document.querySelector('img');
	    var file = this.files[0];
	    var reader = new FileReader();
	
	    reader.addEventListener("load", function () {
	        //preview.src = reader.result;
	        canvasmod.addTheImage(reader.result);
	
	        //hide the file input
	        e.target.style.display = "none";
	    }, false);
	
	    if (file) {
	        reader.readAsDataURL(file);
	    }
	});
	
	exports.getStrokeWidth = getStrokeWidth;
	exports.setStrokeWidth = setStrokeWidth;
	exports.updateObjectOriginals = updateObjectOriginals;
	exports.clearActiveAction = clearActiveAction;
	exports.setSelectedStroke = setSelectedStroke;
	exports.setStrokeToCurrentValue = setStrokeToCurrentValue;
	exports.setCanvas = setCanvas;
	exports.activeAction = activeAction;

/***/ },
/* 4 */
/***/ function(module, exports) {

	'use strict';
	
	var width = 320;
	var height = 320;
	
	//keep the initial width & height for reference
	var originalWidth = width;
	var originalHeight = height;
	
	var lastScaleEventTime = 0;
	
	var currentScaling = 1;
	
	//keeps track of the current scaling, updated while resizing
	
	var canvasWidth = Number(width);
	var canvasHeight = Number(height);
	
	var $canvasContainer = document.querySelector('.canvas-container');
	
	var activeAction = false;
	
	module.exports = {
	    canvasWidth: canvasWidth,
	    canvasHeight: canvasHeight,
	    currentScaling: currentScaling
	};

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var _module_canvas = __webpack_require__(1);
	
	var core = _interopRequireWildcard(_module_canvas);
	
	var _module_paint = __webpack_require__(3);
	
	var paint = _interopRequireWildcard(_module_paint);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	var $strokeSize = document.getElementById('strokeSize');
	
	function changeObjectStroke(obj, stVal) {
	
	    var newStroke = Number(stVal) / Math.max(obj.scaleX, obj.scaleY);
	
	    if (obj.type === "group") {
	
	        var nr = obj.size() - 1;
	        while (nr >= 0) {
	            obj.item(nr).strokeWidth = newStroke;
	            nr--;
	        }
	
	        obj._origStrokeWidth = newStroke;
	    } else {
	
	        obj.strokeWidth = newStroke;
	
	        obj._origStrokeWidth = newStroke;
	    }
	}
	
	function addStrokeListener() {
	
	    $strokeSize.addEventListener("change", function (e) {
	
	        console.log(this.value);
	
	        var newVal = this.value;
	
	        var canvas = core.getCanvas();
	
	        var _strokeWidth = paint.getStrokeWidth();
	
	        if (canvas.getActiveObject()) {
	
	            //if there is active element
	            var obj = canvas.getActiveObject();
	
	            changeObjectStroke(obj, newVal);
	
	            //display change
	            canvas.renderAll();
	        } else if (canvas.getActiveGroup()) {
	
	            canvas.getActiveGroup().forEachObject(function (o) {
	
	                changeObjectStroke(o, newVal);
	            });
	            canvas.renderAll();
	        } else {
	            paint.setStrokeWidth(Number(this.value));
	            canvas.freeDrawingBrush.width = _strokeWidth;
	        }
	    });
	}
	
	module.exports = {
	    addStrokeListener: addStrokeListener
	};

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _module_canvas = __webpack_require__(1);
	
	var core = _interopRequireWildcard(_module_canvas);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	document.getElementById('rotateRight').addEventListener('click', function (e) {
	
	    rotateImage(90);
	});
	
	// ------- functions
	
	function rotateImage(degree) {
	
	    /*
	     when using canvas the canvas itself normaly doesn't change size or rotate
	     but we are changing the size of the of the canvas to fit the image every time the image rotate
	     when canvas changes size all images inside are resized in propotion to the new width and height
	      so:
	     1: we need to rotate the image
	     2: reset the cavas width/height based on the rotation
	     3: scale the image after rotation properly to fit the canvas
	     */
	
	    var mainImage = core.getMainImage();
	
	    var canvas = core.getCanvas();
	
	    var newdeg = mainImage.getAngle() + degree;
	
	    if (newdeg == 360) {
	        newdeg = 0;
	    }
	
	    //2. resize canvas first
	    core.resizeCanvas(canvas.getHeight(), canvas.getWidth());
	
	    //3.scale the image after rotation properly to fit the canvas
	    canvas.centerObject(mainImage);
	
	    mainImage.setAngle(newdeg);
	    mainImage.setCoords();
	
	    /*
	        clipRect.scaleX = currentScaling;
	        clipRect.scaleY = currentScaling;
	        clipRect.top = clipOffset;
	        clipRect.left = clipOffset;
	        clipRect.width = (_canvasWidth - clipOffset*2) / currentScaling;
	        clipRect.height= (_canvasHeight - clipOffset*2) / currentScaling;
	    */
	    canvas.renderAll();
	
	    //core.clipCanvas();
	}

/***/ }
/******/ ]);
//# sourceMappingURL=zooms.entry.js.map