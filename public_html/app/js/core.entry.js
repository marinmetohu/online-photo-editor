/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(7);


/***/ },

/***/ 7:
/***/ function(module, exports) {

	'use strict';
	
	var width = 320;
	var height = 320;
	
	//keep the initial width & height for reference
	var originalWidth = width;
	var originalHeight = height;
	
	var lastScaleEventTime = 0;
	
	var currentScaling = 1;
	
	//keeps track of the current scaling, updated while resizing
	
	var canvasWidth = Number(width);
	var canvasHeight = Number(height);
	
	var $canvasContainer = document.querySelector('.canvas-container');
	
	var activeAction = false;
	
	module.exports = {
	    canvasWidth: canvasWidth,
	    canvasHeight: canvasHeight,
	    currentScaling: currentScaling
	};

/***/ }

/******/ });
//# sourceMappingURL=core.entry.js.map