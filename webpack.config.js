
module.exports = {
    entry: {
        strokeSize: ['./app_es6/module_change_stroke_size.js'],
        module_paint: ['./app_es6/module_paint.js'],
        zooming: ['./app_es6/module_zooming.js'],
        rotate: ['./app_es6/module_rotate.js'],
        canvas: ['./app_es6/core.js']
    },
    output: {
        filename: './public_html/app/js/[name].entry.js'
    },
    devtool: 'source-map',
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'stage-0' ]
                }
            }
        ],
        resolve: {
            extensions: ['.js' ]
        }
    }
};