
import * as core from "./core.js";

import * as stroke from "./module_change_stroke_size.js";

 



//keep the initial width & height for reference

////------------------

var _x = 0;
var _y = 0;
var _width = 100;
var _height = 100;
var _circRadius = 50;

//default border color, when you select the object
var  _borderColor = 'red';
var _strokeColor = 'rgba(255,0,0,1)';
var _strokeWidth = 5;

var _cornerSize = 8;


var $drawingBtns = document.getElementById("drawingBtns");

var $colorpickerCanvas = document.getElementById("colorpickerCanvas");

var $strokeSize  = document.getElementById( 'strokeSize' );

let $addImage = document.getElementById( 'add_picture' );

let $fileinput = document.getElementById( 'fileinput' );

var activeAction = false;

var canvas = canvas;


// initializing default properties
fabric.Object.prototype.set({
    transparentCorners: false,
    cornerColor: 'rgba(102,153,255,1)',
    cornerSize : _cornerSize
});


// customise fabric.Object with a method to resize rather than just scale after tranformation
fabric.Object.prototype.resizeToScale = function () {

    if  (this.type ==='group') {
        this._objects.forEach( function(obj){

            obj.strokeWidth = Math.ceil( obj._origStrokeWidth / Math.max(obj.group.scaleX, obj.group.scaleY) );
        });
    }
    else {
        this.strokeWidth = Math.ceil( this._origStrokeWidth / Math.max(this.scaleX, this.scaleY) );
    }
}

// start
defineColors();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//functions


function defineColors() {

    let colorBtns = document.querySelectorAll('#colorPalette button');
    let colorBtnsSrr = [...colorBtns];

    colorBtnsSrr.forEach( el =>  {

        el.style.backgroundColor = el.value;
    });
}


function updateObjectOriginals (e){

    e.target._left    = e.target.getLeft();
    e.target._top     = e.target.getTop();
    e.target._width   = e.target.getWidth();
    e.target._height  = e.target.getHeight();
    e.target._scaleX  = e.target.scaleX;
    e.target._scaleY  = e.target.scaleY;
}


function setSelectedStroke(nr) {

    if (canvas.getActiveGroup()) {

        //here put -
        $strokeSize.value = ' ';
    }
    else {

        $strokeSize.value = nr;
    }
}

function setStrokeToCurrentValue(){

    setSelectedStroke( _strokeWidth );
}

function disableFreeDraw() {

    canvas.isDrawingMode = false;

    canvas.renderAll();
}

function clearActiveSubmenu() {
    //remove current active
    var active = document.querySelector('.draw_icon.active');

    if(active != null ) active.classList.remove('active');

}

function clearActiveAction() {
    clearActiveSubmenu();
    activeAction = false;
    canvas.isDrawingMode = false;
}

function toggleActive(el) {

    clearActiveSubmenu();

    //add new active
    el.classList.add('active');
}

function setStokeForActiveObject(newColor) {

    if (canvas.getActiveObject()) {

        var obj = canvas.getActiveObject();

        if (obj.type === "group") {

            var nr = obj.size() - 1;

            while (nr >= 0) {

                if (obj.item(nr).stroke !== null) {
                    obj.item(nr).set("stroke", newColor);
                }

                if (obj.item(nr).fill !== null) {
                    obj.item(nr).set("fill", newColor);
                }

                nr--;
            }
            canvas.renderAll();
        }
        else {
            if (obj.stroke !== null) {
                obj.set("stroke", newColor);
            }

            if (obj.fill !== null) {
                obj.set("fill", newColor);
            }

            canvas.renderAll();
        }
    }
    else if (canvas.getActiveGroup()) {

        canvas.getActiveGroup().forEachObject(function (o) {

            o.set("stroke", newColor);

            if (o.fill !== null) {
                o.set("fill", newColor);
            }
        });
        canvas.renderAll();
    }
    else {

        //if there's no object selected
        //then change the default stroke color
        _strokeColor = newColor;
        canvas.freeDrawingBrush.color = newColor;
    }
}

function drawText() {
    
    var itext = new fabric.IText("write here...", {
        left: _x,
        top: _y,
        fontSize: 20,
        fill: _strokeColor,
        originX: "center",
        originY: "center",
        strokeWidth: 0
    });
    canvas.add(itext);
}

function drawSquare() {

    //first disable free draw mode
    disableFreeDraw();

    // create a rectangle object
    var rect = new fabric.Rect({
        left: _x,
        top: _y,
        fill: null,
        width: _width,
        height: _height,
        borderColor: _borderColor,
        strokeWidth: _strokeWidth,
        _origStrokeWidth: _strokeWidth,
        stroke: _strokeColor,
        originX: "center",
        originY: "center",
        'selectable': true
    });

    // "add" rectangle onto canvas
    canvas.add(rect);

}

function drawCircle() {
    //console.log("draw circle");

    //first disable free draw mode
    disableFreeDraw();

    // create a rectangle object
    let Circle = new fabric.Circle({
        left: _x,
        top: _y,
        fill: null,
        radius: _circRadius,
        strokeWidth: _strokeWidth,
        stroke: _strokeColor,
        borderColor: _borderColor,
        originX: "center",
        originY: "center",
        'selectable': true
    });

    // "add" rectangle onto canvas
    canvas.add(Circle);

    canvas.bringToFront(Circle);

    canvas.renderAll();

}

function drawLine() {
    //console.log("draw line");

    //first disable free draw mode
    disableFreeDraw();


    var line = new fabric.Line([0, 100, 0, 0], {
        left: _x,
        top: _y,
        strokeWidth: _strokeWidth,
        stroke: _strokeColor,
        borderColor: _borderColor,
        fill: null,
        'selectable': true
    });

    canvas.add(line);
}

function drawFreely() {

    // **Enables line drawing
    canvas.isDrawingMode = true;

    //set properties
    canvas.freeDrawingBrush.width = _strokeWidth;
    canvas.freeDrawingBrush.color = _strokeColor;

}

function drawArrow() {

    //first disable free draw mode
    disableFreeDraw();

    var line,
        arrow,
        circle,
        deltaX,
        deltaY;

    line = new fabric.Line([0, 0, 100, 0], {
        stroke: _strokeColor,
        selectable: true,
        strokeWidth: '3',
        padding: 5,
        hasBorders: false,
        hasControls: false,
        originX: 'center',
        originY: 'center'
    });

    var centerX = (line.x1 + line.x2) / 2,
        centerY = (line.y1 + line.y2) / 2;
    deltaX = line.left - centerX,
        deltaY = line.top - centerY;

    arrow = new fabric.Triangle({
        left: line.get('x1') + deltaX,
        top: line.get('y1') + deltaY,
        originX: 'center',
        originY: 'center',
        hasBorders: false,
        hasControls: false,
        pointType: 'arrow_start',
        strokeWidth: 0,
        angle: -90,
        width: 20,
        height: 20,
        fill: _strokeColor
    });

    circle = new fabric.Circle({
        left: line.get('x2') + deltaX,
        top: line.get('y2') + deltaY,
        radius: 3,
        strokeWidth: 3,
        originX: 'center',
        originY: 'center',
        hasBorders: false,
        hasControls: false,
        pointType: 'arrow_end',
        stroke: _strokeColor
    });

    line.customType = arrow.customType = circle.customType = 'arrow';


    var group = new fabric.Group([line, circle, arrow], {
        left: _x,
        top: _y,
        strokeWidth: 3
    });

    canvas.add(group);

}


function deleteActive() {

    if (canvas.getActiveObject()) {

        var actobj = canvas.getActiveObject();

        canvas.remove(actobj);
    }
    else if (canvas.getActiveGroup()) {

        canvas.getActiveGroup().forEachObject(function (o) {
            canvas.remove(o)
        });
        canvas.discardActiveGroup().renderAll();
    }

}


document.getElementById('trash_can').addEventListener('click', function () {

    if( this.classList.contains("active") ){
        deleteActive();
    }
});

$drawingBtns.addEventListener('click', function ( e ) {
    // console.log(e);
    e.preventDefault();
    e.stopPropagation();

    var el = e.target;
    var id = el.getAttribute('id');

    //if not parent
    if(id != "drawingBtns"){

        if(el.classList.contains("active") ){

            el.classList.remove("active");

            //reset active action
            activeAction = false;

            disableFreeDraw();
        }
        else{

            if(canvas.isDrawingMode == true){
                //make sure to always disable free draw
                disableFreeDraw();
            }

            toggleActive( el );
            activeAction = id;

            if(id == "add_picture"){
                
            }
        }
    }
});

document.getElementById('setColor').addEventListener(  'click',   function ( e ) {


    if( e.target.tagName == "BUTTON"){

        var el = e.target;

        var newColor = new fabric.Color(el.value).toRgb();

        setStokeForActiveObject( newColor );

        //if there are no selected objects, then set the active color
        if(!canvas.getActiveObject() && !canvas.getActiveGroup() ) {

            let parent = el.parentNode;

            parent.querySelector(".active").classList.remove("active");

            el.classList.add("active");
        }
    }

});


function executeAction( ) {

    switch (activeAction) {

        case "draw_text":
            drawText();
            break;

        case "draw_circle":
            drawCircle();
            break;

        case "draw_square":
            drawSquare();
            break;

        case "draw_line":
            drawLine();
            break;

        case "draw_arrow":
            drawArrow();
            break;

        case "draw_freely":
            drawFreely();
            break;

        default:
            break;
    }
}


function uploadImage(){
    console.log('upload');
}

/*///-----  GETTERS and SETTERS  -----///*/

var getStrokeWidth = function(){

    return _strokeWidth;
}
var setStrokeWidth = function(nr){

    _strokeWidth = nr;
}

function setCanvas(_canvas) {

    canvas = _canvas;

    canvas.observe('mouse:down' , function (  opt ) {

        if(activeAction !== false) {

             _x = opt.e.offsetX /  core.currentScaling;
             _y = opt.e.offsetY /  core.currentScaling;

            executeAction( );
        }

    });

    //add stroke listener
    stroke.addStrokeListener();
}


$colorpickerCanvas.addEventListener( "input", function (e ) {

    var newColor = new fabric.Color( this.value ).toRgb();

    setStokeForActiveObject( newColor );
});



$fileinput.addEventListener( "change", function ( e ) {

    //console.log(e, this);
    //let preview = document.querySelector('img');
    let file    = this.files[0];
    let reader  = new FileReader();

    reader.addEventListener("load", function () {
        //preview.src = reader.result;
        core.addTheImage(reader.result);

        //hide the file input
        e.target.style.display = "none";
 

    }, false);

    if (file) {
        reader.readAsDataURL(file);
    }
});



export {
    getStrokeWidth,
    setStrokeWidth,
    updateObjectOriginals,
    clearActiveAction,
    setSelectedStroke,
    setStrokeToCurrentValue,
    setCanvas,
    activeAction
}