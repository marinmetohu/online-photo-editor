

import * as core from "./core.js";
import * as zoom from "./module_zooming.js";


document.getElementById('rotateRight').addEventListener('click', function(e) {

    rotateImage(90);
});


// ------- functions

function rotateImage(degree) {

    /*
     the canvas itself normally shouldn't change size or rotate
     but we are changing the size of the of the canvas to fit the image every time the image rotates
     when canvas changes size all images inside are repositioned in propotion to the new width and height

     1: we need to rotate and reposition all elements
     2: reset the cavas width/height based on the rotation
     */

    let mainImage = core.getMainImage();

    let canvas = core.getCanvas();

    let currWidth  = canvas.getWidth();
    let currHeight = canvas.getHeight();

    let newHeight = currWidth;
    let newWidth  = currHeight;

    let newdeg = mainImage.getAngle() + degree;

    let currZoomFactor = zoom.getZoomFactor();

    let objects = canvas.getObjects();


    for (let obj of objects) {

        obj.set({
            originX :"center",
            originY : "center",
            centeredRotation : true
        });

        let imgNewTop  = 0;
        let imgNewLeft = 0;

        let imgLeft = obj.getLeft();
        let imgTop  = obj.getTop();

        //the old bottom serves as new left
        imgNewLeft = Math.round( ( newWidth - imgTop   ) * 100 ) / 100 ;

        //left becomes top
        imgNewTop  = Math.round(  imgLeft   * 100 ) / 100 ;


        if(newdeg == 360){
            newdeg = 0;
        }

        obj.setLeft(  imgNewLeft );
        obj.setTop(  imgNewTop  ) ;

        //update original top left needed in zooming
        obj._left = Math.round( (imgNewLeft/currZoomFactor) * 100 ) /100;
        obj._top = Math.round( (imgNewTop /currZoomFactor) * 100 ) /100;

        obj.setAngle(newdeg) ;
        obj.setCoords();
    }

    //2. resize canvas first
    core.resizeCanvas( newWidth, newHeight);

    core.flipCanvasOriginals( );

    canvas.renderAll();

}
