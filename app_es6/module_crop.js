
import * as core from "./core.js";
import * as paint from "./module_paint.js";
import * as zoom from "./module_zooming.js";

let cropRect = false;

let cropOffset = 0;

let isScaling = false;

let _lastScaleEventTime = 0;

let _cleft,
    _ctop,
    _cwidth,
    _cheight,
    canvas,
    canvasWidth,
    canvasHeight,
    rectTop,
    rectBottom,
    rectLeft,
    rectRight;

let $crop         = document.getElementById('crop');
let $cancelCrop   = document.getElementById('cancelCrop');
let $applyCrop    = document.getElementById('applyCrop');
let $subMenu      = document.getElementById('subMenu');

let $container    = document.getElementById('container');


$crop.addEventListener('click', function(e) {

    if(cropRect == false){

        addCropRectangle( );

        $subMenu.style.display = "block";

        $container.classList.add( "crop" );
    }
});

$cancelCrop.addEventListener('click', function(e) {

    removeCropping();
});

$applyCrop.addEventListener('click', function(e) {

    //reposition all objects based on crop area
    positionObjects();

    //crop and resize the canvas
    cropCanvas();
});

function addCropRectangle() {

    canvas       = core.getCanvas();
    canvasWidth  = canvas.getWidth();
    canvasHeight = canvas.getHeight();

    _cleft   = cropOffset;
    _ctop    = cropOffset;
    _cwidth  = canvasWidth - (cropOffset * 2);
    _cheight = canvasHeight - (cropOffset * 2);


    cropRect = new fabric.Rect({
        left: _cleft,
        top: _ctop,
        fill: '',
        width: _cwidth,
        height: _cheight,
        lockRotation: true,
        borderColor: '',
        cornerColor: 'red',
        cornerSize : 16,
        transparentCorners: false,
        selectable: true,
        lockScalingFlip : true,
        isclipper : true,
        centeredScaling: false
    });

    canvas.add(cropRect);

    createShadowRects();

    canvas.renderAll();

    canvas.bringToFront(cropRect).setActiveObject(cropRect);


    cropRect.on('scaling', function(event) {

        var diff = event.e.timeStamp - _lastScaleEventTime;

        if(diff > 20){

            _lastScaleEventTime = event.e.timeStamp;
            calcCroppingScale(this, event);

            updateShadowRects(this);
        }
    });
    cropRect.on('moving', function() {

        calcCroppingMove(this);

    });

}

function createShadowRects() {

    let _top    = cropRect.getTop() - cropOffset;
    let _left   = cropRect.getLeft() - cropOffset;
    let _width  = cropRect.getWidth() + cropOffset*2;
    let _height = cropRect.getHeight() + cropOffset*2;

    let opts = {
        fill: "rgba(0,0,0,0.8)",
        lockRotation: true,
        transparentCorners: true,
        selectable: false,
        lockScalingFlip : true,
        centeredScaling: false
    }

    rectLeft = new fabric.Rect({
        left: 0,
        top: 0,
        width: _left,
        height: canvasHeight
    });

    rectTop = new fabric.Rect({
        left: 0,
        top: 0,
        width: canvasWidth,
        height: _top,
        originY : "top",
        originX : "left"
    });

    rectBottom = new fabric.Rect({
        left: 0,
        top:   (_top + _height ) ,
        width: canvasWidth,
        height: canvasHeight - (_top + _height),
        originY : "bottom"
    });

    rectRight = new fabric.Rect({
        left: (_left + _width),
        top: 0,
        width: canvasWidth - ( _left + _width ),
        height: canvasHeight,
        originX : "left"
    });

    rectTop.set(opts);
    rectBottom.set(opts);
    rectLeft.set(opts);
    rectRight.set(opts);

    canvas.add(rectTop).add(rectBottom).add(rectLeft).add(rectRight);

    canvas.renderAll();
}

function updateShadowRects(ob) {

    let _top    = cropRect.getTop() - cropOffset;
    let _left   = cropRect.getLeft() - cropOffset;
    let _width  = cropRect.getWidth() + cropOffset*2;
    let _height = cropRect.getHeight() + cropOffset*2;

    //----  top rectangle
    rectTop.setHeight( round2dec( _top ) );
    rectTop.setCoords();

    //----  bottom rectangle
    rectBottom.set({
        height : round2dec( canvasHeight - (_top + _height ) )
    });
    rectBottom.setCoords();

    //----  left rectangle
    rectLeft.setWidth(round2dec( _left ));
    rectLeft.setCoords();

    //----  right rectangle
    let rleft = round2dec(  _left + _width  );
    rectRight.set({
        width : canvasWidth - rleft,
        left : rleft
    });
    rectRight.setCoords();

    canvas.renderAll();
}



function calcCroppingScale(ob, event) {
    // than bound to canvas size
    //don't let it scale beyond
    var cw =   canvas.getWidth();
    var ch =   canvas.getHeight();

    var pointer = canvas.getPointer(event.e);
    var posX = pointer.x;
    var posY = pointer.y;

    if( (posY-cropOffset) < 0 || (posY+cropOffset) > ch){

        ob.lockScalingY = true;

        //console.log(ob);

        if( (posY-cropOffset) <  0){
            ob.set({
                top : cropOffset,
            });

            ob.setCoords();

            if(ob.getHeight() > ch){
                ob.scaleToHeight( (ch-cropOffset*2 ) );
                ob.set({
                    scaleX : ob.originalState.scaleX,
                });
                ob.setCoords();
            }
        }

    }
    else{
        ob.lockScalingY = false;
    }

    if( (posX-cropOffset) < 0 || (posX+cropOffset) > cw){

        ob.lockScalingX = true;

        if((posX-cropOffset) < 0) {

            ob.set({
                left : cropOffset
            });

            ob.setCoords();

            if(ob.getHeight() > cw){
                ob.scaleToWidth( (cw-cropOffset*2 ) );
                ob.set({
                    scaleY : ob.originalState.scaleY,
                });
                ob.setCoords();
            }

        }

        if( (posX + cropOffset ) > cw ){

            ob.scaleToWidth(cw - cropOffset*2 );
        }
    }
    else{
        ob.lockScalingX = false;
    }
}

function calcCroppingMove(ob) {

    var obw  = Math.round( ob.width  * ob.scaleX  )  ;
    var obh = Math.round( ob.height * ob.scaleY )  ;

    var cw =   canvas.getWidth();
    var ch =   canvas.getHeight();

    //constrain movement top bottom
    if( (ob.top - cropOffset) <  0){
        ob.top = cropOffset;
    }
    if( (obh + ob.top + cropOffset*2) >= ch  ){
        ob.top = ch - obh - cropOffset;
    }

    //constrain movement left right
    if( (ob.left-cropOffset) < 0){
        ob.left = cropOffset;
    }
    else if( (obw + ob.left + cropOffset*2) >= cw ){
        ob.left = cw  - obw - cropOffset;
    }

    ob.setCoords();
    updateShadowRects(ob);

    canvas.renderAll();
}

function positionObjects() {

    //first deactive all objects
    canvas.deactivateAll();

    let leftCrop   = cropRect.getLeft();
    let topCrop    = cropRect.getTop();

    let zoomFactor = zoom.getZoomFactor();

    if (canvas.backgroundImage) {
        // Need to scale background images as well
        let bi = canvas.backgroundImage;
        bi.width = bi.width * factor;
        bi.height = bi.height * factor;
    }

    let objects = canvas.getObjects();

    for (let o of objects) {

        let left = o.getLeft();
        let top  = o.getTop();

        let tempLeft  = round2dec( (left - leftCrop)  );
        let tempTop   = round2dec( (top - topCrop )  );

        o.setLeft( tempLeft );
        o.setTop( tempTop );

        //update objects oroginal left top
        o._left = round2dec( tempLeft / zoomFactor);
        o._top  = round2dec( tempTop / zoomFactor );

        o.setCoords();
    }

    canvas.calcOffset();
    canvas.renderAll();
}

function cropCanvas(){

    let zoomFactor = zoom.getZoomFactor();

    if(cropRect == null || typeof cropRect == "undefined"){
        return false;
    }
    _cwidth  = round2dec( cropRect.getWidth()  + cropOffset*2 ) ;
    _cheight = round2dec( cropRect.getHeight() + cropOffset*2 ) ;

    // clips the canvas based on this rectangle area
    canvas.clipTo = function (ctx) {
        ctx.rect(0, 0, _cwidth, _cheight );
    }

    // remove cropping rectangle
    removeCropping();

    // remove 
    core.resizeCanvas(_cwidth, _cheight);
    core.resetCanvasOriginals(round2dec(_cwidth / zoomFactor), round2dec( _cheight / zoomFactor ) );

    canvas.renderAll();

    canvas = core.getCanvas();

}

function removeCropping() {

    paint.clearActiveAction();

    if(cropRect != false){

        cropRect.selectable = false;

        cropRect.off('scaling');
        cropRect.off('moving');

        canvas.remove( cropRect );
        canvas.remove( rectTop );
        canvas.remove( rectBottom );
        canvas.remove( rectLeft );
        canvas.remove( rectRight );

        cropRect = false;

        rectTop    = null;
        rectLeft   = null;
        rectBottom = null;
        rectRight  = null;

        $subMenu.style.display  = "none";

        $container.styleList.remove( "crop" );
    }

    canvas.deactivateAllWithDispatch().renderAll();

}

function round2dec(nr){

    return  Math.round( nr * 100) / 100;
}

export{
    addCropRectangle,
    removeCropping
}