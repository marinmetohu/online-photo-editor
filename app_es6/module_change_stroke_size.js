
import * as core from "./core.js";

import * as paint from "./module_paint.js";

var $strokeSize  = document.getElementById( 'strokeSize' );


function changeObjectStroke(obj, stVal){

    var newStroke = Number(stVal) / Math.max(obj.scaleX, obj.scaleY);

    if(obj.type === "group"){

        var nr = obj.size() - 1;
        while(nr >= 0){
            obj.item(nr).strokeWidth = newStroke;
            nr--;
        }

        obj._origStrokeWidth = newStroke;
    }
    else{

        obj.strokeWidth = newStroke;

        obj._origStrokeWidth = newStroke;
    }
}

function addStrokeListener(  ) {

    $strokeSize.addEventListener( "change", function ( e ) {

        console.log( this.value );

        let newVal = this.value;

        let canvas = core.getCanvas();

        let _strokeWidth = paint.getStrokeWidth();

        if(canvas.getActiveObject()    ) {

            //if there is active element
            var obj = canvas.getActiveObject();

            changeObjectStroke(obj, newVal);

            //display change
            canvas.renderAll();
        }
        else if(canvas.getActiveGroup() ){

            canvas.getActiveGroup().forEachObject( o => {

                changeObjectStroke(o, newVal);

            });
            canvas.renderAll();
        }
        else{
            paint.setStrokeWidth( Number(this.value) );
            canvas.freeDrawingBrush.width = _strokeWidth;
        }
    });
}

module.exports = {
    addStrokeListener : addStrokeListener
}