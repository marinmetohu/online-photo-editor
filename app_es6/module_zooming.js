
import * as core from "./core.js";

let $currZoom =  document.getElementById( "currentZoom" );

let currFactor = 1;

function listenZoom() {


    let resizeControl = document.getElementById("resizeControl");

    if (typeof resizeControl != "undefined" && resizeControl != null) {

        resizeControl.addEventListener('change', function(result) {


            let delta = Number(this.value);

            let factor = Math.round( (1 + delta) * 100 ) / 100;

            currFactor = factor;

            zoomIt( factor );

            $currZoom.innerText = factor;

        });
    }
}
/////

function zoomIt( factor ) {

console.log(factor);

    let canvas = core.getCanvas();

    let cwidth  = Math.round(core.originalWidth * factor * 100) / 100;
    let cheight = Math.round(core.originalHeight * factor * 100) / 100;


    if (canvas.backgroundImage) {
        // Need to scale background images as well
        var bi = canvas.backgroundImage;
        bi.width = bi.width * factor;
        bi.height = bi.height * factor;
    }

    var objects = canvas.getObjects();

    for (var i in objects) {
        /*
        var scaleX = objects[i].scaleX;
        var scaleY = objects[i].scaleY;
        var left = objects[i].getLeft();
        var top = objects[i].getTop();
        */
        var _left = objects[i]._left;
        var _top = objects[i]._top;
        var _scaleX= objects[i]._scaleX;
        var _scaleY = objects[i]._scaleY;

        var tempScaleX  = Math.round( ( _scaleX * factor) * 100) / 100;
        var tempScaleY  = Math.round( ( _scaleY * factor) * 100) / 100;
        var tempLeft    = Math.round( (_left * factor) * 100) / 100;
        var tempTop     = Math.round( (_top * factor) * 100) / 100;

        console.log(_scaleX, _scaleY, tempScaleX, tempScaleY);


        if( !objects[i].isclipper ){

            objects[i].setLeft( tempLeft );
            objects[i].setTop( tempTop );

            objects[i].scaleX = tempScaleX;
            objects[i].scaleY = tempScaleY;
        }

        objects[i].setCoords();
    }


    canvas.calcOffset();
    canvas.renderAll();

    core.resizeCanvas(cwidth, cheight);

}

function getZoomFactor(   ){

    return currFactor;
}

export { listenZoom,
        getZoomFactor
}
