

import * as paint from "./module_paint.js";
import * as zoom from "./module_zooming.js";
import * as crop from "./module_crop.js";
import * as rotatemod from "./module_rotate.js";

let currentScaling  = 1;
let imgInstance     = false;
let canvas          = false;
let originalWidth   = 0;
let originalHeight  = 0;
let canvasWidth     = 0;
let canvasHeight    = 0;

let $container = document.getElementById("container");

function createCanvas( w, h ) {

    originalWidth   = w;
    originalHeight  = h;
    canvasWidth     = w;
    canvasHeight    = h;

    //create canvas
    canvas = new fabric.Canvas('drawingCanvas',{
        width:  w,
        height:  h
    });

    //canvas observe

    canvas.observe('object:added', function (e) {
        console.log("added", paint);
        e.target._origStrokeWidth = e.target.strokeWidth;

        paint.updateObjectOriginals(e);
    });

    canvas.observe('object:moving', function (e) {

        paint.updateObjectOriginals(e);
    });

    canvas.observe('object:selected', function (e) {

        //
        document.querySelector('#trash_can').classList.add("active");

        //
        paint.clearActiveAction();

        //
        paint.setSelectedStroke( Math.ceil( e.target.strokeWidth ) );

    });

    canvas.observe('selection:cleared', function (e) {

        document.querySelector('#trash_can').classList.remove("active");

        paint.setStrokeToCurrentValue();
    });

    canvas.on('object:scaling', function (e) {

        e.target._scaleX = e.target.scaleX ;
        e.target._scaleY = e.target.scaleY ;

        e.target._left   = e.target.getLeft();
        e.target._top    = e.target.getTop();

        if(e.target.isclipper === false){
            e.target.resizeToScale();
        }
    });

    $container.classList.add("opened");

    document.getElementById("save").addEventListener('click', function (e) {

        e.preventDefault();

        saveCanvas();
    });
}

function addImageToCanvas( img ) {

    imgInstance = img;

    imgInstance.set({

        left:  originalWidth/2,
        top:  originalHeight/2,
        angle: 0,
        opacity: 1,
        selectable : false,
        lockScalingX : true,
        lockScalingY : true,
        lockMovementX : true,
        lockMovementY : true,
        centeredRotation: true,
        centeredScaling: true,
        mainImage   : true,
        originX     : "center",
        originY     : "center"
    });

    canvas.add( imgInstance);

    canvas.sendToBack( imgInstance);

    canvas.renderAll();
}

var addTheImage = function (url ){


    //add the image
    //var imgElement = $('.text-center').find('img#image');

    fabric.Image.fromURL(url , function (img) {


        createCanvas( img.getWidth() , img.getHeight() );

        addImageToCanvas( img );

        paint.setCanvas(  canvas );

        zoom.listenZoom();

    });

}

function resizeCanvas( w, h) {

    let $canvasContainer = document.querySelector('#imageContainer .canvas-container');
    
    //update variables
    canvasWidth = w;
    canvasHeight= h;

    //update canvas elements
    canvas.setDimensions({width:w, height:h} );

    //update CANVASes in dom
    let cns = $canvasContainer.querySelectorAll('canvas');
    let cnsarr = [...cns];

    cnsarr.forEach( c =>  {
        c.style.width  = w;
        c.style.height  = h;
    });

    $canvasContainer.style.width  = w;
    $canvasContainer.style.height = h;

    canvas.clipTo = function (ctx) {
        ctx.rect(0, 0, w, h );
    }
    canvas.renderAll();

}

function resetCanvasOriginals(width, height) {

    originalWidth  = width;
    originalHeight = height;
}

function flipCanvasOriginals( ) {

    let w = originalWidth;
    let h = originalHeight;

    originalWidth  = h;
    originalHeight = w;
}


function saveCanvas() {

    let scale = zoom.getZoomFactor();

    let realScale = Math.round( 1/scale * 100 ) / 100;

    let img = canvas.toDataURL({
        format: 'png',
        left: 0,
        top: 0,
        width: canvasWidth,
        height: canvasHeight,
        multiplier: realScale
    });

    window.open( img );
}


/*///  GETTERS &  SETTERS ///*/

function getMainImage() {
    return imgInstance;
}
function getCanvas() {
    return canvas;
}

export {

    addTheImage,
    currentScaling,
    imgInstance,
    canvas,
    originalWidth,
    originalHeight,
    canvasWidth,
    canvasHeight,
    resizeCanvas,
    getMainImage,
    getCanvas,
    resetCanvasOriginals,
    flipCanvasOriginals
}