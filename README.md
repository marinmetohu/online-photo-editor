# HTML5 Online Canvas Photo Editor - usint ES6

To check it out just open /public_html/index.html

#### Libraries used

FabricJs for Canvas 

BabelJs for ES6 to ES5 translation

WebPack for automatisation 

#### ES6 Usage
Run ‘webpack’ from the command line in the root directory

Run ‘webpack --watch’ for live translation